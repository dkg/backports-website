[[!toc  startlevel=2 levels=2]]

##Packaging

###Introduction

The packages on debian-backports are made by volunteers. If you like to help
with backporting packages from the Debian archive, make sure you follow these
guidelines or ask on the [mailing list] for clarification.

Backports are about additional features that are only offered in a new version,
not a replacement for getting fixes into stable - use stable-updates for that.
Backports tracks testing and only package versions included in testing are
allowed in it, subject to a few expedient exceptions.

##Eligibility

If you feel you would need to diverge from these rules, either discuss it on
the [mailing list] or bring it up with the [Backports Team] for an exception.

[mailing list]: https://lists.debian.org/debian-backports/
[Backports Team]: mailto:backports-team@debian.org

###Package

* Before uploading please think about how useful the package is for stable
  users and if it is sensible to support the package for the full supported
  lifetime of a release cycle (stable and oldstable).
* Don't backport minor version changes without user visible changes or bugfixes
* To guarantee an upgrade path from stable+backports to the next stable, the
  package needs to be in testing. Security updates may be uploaded before the
  testing migration completes. Other short-term exceptions can be granted for
  e.g. library transitions, critical bugs on request from the [Backports Team].
* Please only upload package with a notable userbase. Backports is not a
  maintainer's PPA; a user request for the package may be an indicator. Use
  common sense and if in doubt, ask about the package on the [mailing list].
* Check the [backports NEW queue] before starting to avoid duplicated effort.
* If you want to update an existing outdated backport, please inform the person
  who [backported] the last accepted version about your intentions.

[backports NEW queue]: https://ftp-master.debian.org/backports-new.html
[backported]: https://backports.debian.org/changes/stretch-backports.html

###Maintainer

Please note, that you are responsible for this backport from the time on when
it was accepted on debian-backports. This means, you have to keep track of the
changes in unstable, update your backport when a new version enters testing and
provide security updates when needed. If you are not willing or capable of
doing this, you better ask someone else (e.g. on the [mailing list]) to create
and maintain the backport.

An Uploader for the unstable package is the ideal person to maintain a backport
since they're already following the testing migration, so please contact them
in the first instance (e.g. by raising a wishlist bug). However it is by no
means required - perhaps they're not interested in supporting the full release
cycle, but they know about complicated dependencies you should discuss.

* You must be subscribed to the [mailing list] to receive relevant
  announcements and bug reports.
* A Debian Developer (DD) uid needs to be in the backports ACL.
  This can be requested by raising a ticket in the [backports queue] on the [RT].
* Debian Maintainers (DM) can also get their uid into the backports ACL.
  This can be requested by raising a ticket in the [backports queue] on the [RT].
  * As with unstable, a [backports NEW queue] package needs a sponsor.
  * Similarly, you must have been [granted upload permission] for each package,
    but if you're not the unstable maintainer please talk to them first.
* Other Debian contributors should upload to e.g. [mentors] and ask for
  sponsorship on the [mailing list].
* It is recommended to subscribe to the bug reports using the package [tracker]
  to be notified about issues that potentially affect the backport too.
* If you put yourself into Uploaders: in debian/control you can easily track
  your backports on the [package overview], or use its subscribe feature.

[backports queue]: git@salsa.debian.org:backports-team/backports-website.git
[RT]: https://wiki.debian.org/rt.debian.org
[granted upload permission]: https://wiki.debian.org/DebianMaintainer#Granting_Permissions
[mentors]: https://mentors.debian.net/
[tracker]: https://tracker.debian.org/
[package overview]: https://qa.debian.org/developer.php

###Supported Distributions

The following distributions are supported for backports. Please don't use
unstable or stable as target distribution. Append "~bpo${release}+${build}" to
the version number, e.g. "1.2-3" becomes "1.2-3~bpo9+1" (or use `dch --bpo`).

| Source Distribution | Backports Distribution                       |
|---------------------|----------------------------------------------|
| buster              | stretch-backports                            |

With the release of a new stable version uploading packages with versions
greater than in new stable or new stable-security are not allowed. So if you
want to upload a new package version from e.g. bullseye to stretch, use
stretch-backports-sloppy as the target distribution.

##Packaging

* Do not make any changes to the packaging unrelated to backporting. Keep the
  diff between the testing and the backports versions as minimal as possible.
* Make sure that you have a proper build environment that only contains the
  suite for which the backport is for and no unneeded backports. Maybe you want
  to consider pbuilder or cowbuilder for building packages.
* Document all changes you needed to do in order to make it run on stable.
* Please submit your changes to the debian VCS-\* (or ask the maintainer to).
* It is recommended to include all changelog entries since the last version on
  debian-backports or since stable if it's the first version. You should do
  this by passing "-v<version>" to dpkg-buildpackage. Eg: "debuild -v0.7.5-2",
  where "0.7.5-2" is the version in stable.
* Backports of an updated version of a package that was backported before *may*
  have a changelog that merges entries of backports of previous versions, but
  this is **not required**.
* Do not lower the standards version, it is just useless.
* Do not add lintian overrides to suppress the 'wrong-distribution' warnings.

##Security Uploads

If you upload a package which fixes a security related problem please create a
ticket in the [backports queue] on the [RT].

Please follow the following template and provide us with the required
information to write a BSA. Please don't wait for the BSA and upload
immediately.

    Subject: [BSA-XXX] Security Update for <packagename>

    <Uploader> uploaded new packages for <packagename> which fixed the
    following security problems:

    CVE-XXXX or whatever ID if existant
      short description
      ...
    CVE-....
      ....

    For the stretch-backports distribution the problems have been fixed in
    version <packageversion>.

    <other distributions if any>
